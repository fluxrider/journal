package io.github.fluxrider.util;

#include "way_too_many_imports" // see preprocessor.gradle

public class SequencePicker extends LinearLayout {

  public interface ClickCallback {
    void update(Object value);
  }

  public interface Click {
    void pick_new_value(Object current, ClickCallback onValue);
  }

  public interface Next {
    Object gen_next(Object current, boolean previous);
  }

  public interface Formatter {
    String format(Object o);
  }

  public Object current_value;
  public String toString() { return current_value.toString(); }

  public Thread prev_held = null;
  public Thread next_held = null;

  public SequencePicker(Context context, Object v, Click click_cb, Next next_cb, Formatter f) {
    super(context);
    LinearLayout.LayoutParams params;

    TextView text_view = new TextView(context);
    params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.WRAP_CONTENT,
      ViewGroup.LayoutParams.WRAP_CONTENT
    );
    params.weight = 0;
    params.gravity = Gravity.CENTER;
    text_view.setLayoutParams(params);
    current_value = v;
    text_view.setText(f.format(v));
    text_view.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
    text_view.setGravity(Gravity.CENTER);
    text_view.setOnClickListener((View view) -> {
      click_cb.pick_new_value(current_value, (Object neo) -> {
        current_value = neo;
        text_view.setText(f.format(neo));
      });
    });

    TextView prev = new HeldText(context, 100, 10, () -> {
      current_value = next_cb.gen_next(current_value, true);
      text_view.setText(f.format(current_value));
    });
    params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.WRAP_CONTENT,
      ViewGroup.LayoutParams.WRAP_CONTENT
    );
    params.weight = 1;
    params.gravity = Gravity.CENTER;
    prev.setLayoutParams(params);
    prev.setText("<");
    prev.setGravity(Gravity.CENTER);

    TextView next = new HeldText(context, 100, 10, () -> {
      current_value = next_cb.gen_next(current_value, false);
      text_view.setText(f.format(current_value));
    });
    params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.WRAP_CONTENT,
      ViewGroup.LayoutParams.WRAP_CONTENT
    );
    params.weight = 1;
    params.gravity = Gravity.CENTER;
    next.setGravity(Gravity.CENTER);
    next.setLayoutParams(params);
    next.setText(">");

    // style
    text_view.setTextSize(24);
    prev.setTextSize(32);
    next.setTextSize(32);

    // layout
    this.setOrientation(LinearLayout.HORIZONTAL);
    this.addView(prev);
    this.addView(text_view);
    this.addView(next);
  }

}
