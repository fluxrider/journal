// By default, preference don't show their value (except for check boxes).
// Here I use the summary field to animate the value and actual summary of Preference.
// The purpose of this class is to hide this mess from an actual Preference class.

package io.github.fluxrider.util;

#include "way_too_many_imports" // see preprocessor.gradle

public abstract class PreferenceFrag extends PreferenceFragmentCompat {

  private Thread animate;
  private List<Preference> prefs = new ArrayList<>();
  private List<String> summaries = new ArrayList<>();

  public void animate(Preference pref, String summary) {
    prefs.add(pref);
    summaries.add(summary);
  }

  public void onResume() {
    super.onResume();
    animate = new Thread(() -> {
      while(animate == Thread.currentThread()) {
        getActivity().runOnUiThread(() -> {
          if(System.currentTimeMillis() / 2000 % 2 == 0) {
            for(int i = 0; i < prefs.size(); i++) {
              String value = "";
              if(prefs.get(i) instanceof EditTextPreference)
                value = ((EditTextPreference)prefs.get(i)).getText();
              prefs.get(i).setSummary(value);
            }
          } else {
            for(int i = 0; i < prefs.size(); i++) {
              prefs.get(i).setSummary(summaries.get(i));
            }
          }
        });
        SystemClock.sleep(100);
      }
    });
    animate.start();
  }

  public void onPause() {
    animate = null;
    super.onPause();
  }

}
