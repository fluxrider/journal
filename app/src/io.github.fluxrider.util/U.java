package io.github.fluxrider.util;

// The purpose of this class is to provide short one-liner calls to things that
// would really be short one liners to begin with.
// (e.g. PreferenceManager.getDefaultSharedPreferences() is not short).

#include "way_too_many_imports" // see preprocessor.gradle

public final class U {

  private U() { }

  public static void runOnUiThread(Runnable r) {
    // no need for an activity instance
    Handler handler = new Handler(Looper.getMainLooper());
    handler.post(r);
  }

  public static void toast(Context context, String message) {
    runOnUiThread(() -> {
      Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    });
  }

  public static void notification(Context context, String title, Throwable t) {
    StringWriter buffer = new StringWriter();
    t.printStackTrace(new PrintWriter(buffer));
    notification(context, title, buffer.toString());
  }

  public static void notification(Context context, String title, String message) {
    // create notification channel
    NotificationChannel channel = new NotificationChannel(
      "io.github.fluxrider.channel",
      "Journal",
      NotificationManager.IMPORTANCE_LOW
    );
    channel.setDescription("Journal");
    channel.enableLights(false);
    channel.enableVibration(false);

    // notification
    Notification notification =
      new Notification.Builder(context, "io.github.fluxrider.channel")
      .setContentTitle(title)
      .setStyle(new Notification.BigTextStyle().bigText(message))
      .setSmallIcon(io.github.fluxrider.journal.R.drawable.app_icon)
      .build();

    // create channel and send notification
    NotificationManager manager = (NotificationManager)
      context.getSystemService(Context.NOTIFICATION_SERVICE);
    manager.createNotificationChannel(channel);
    manager.notify(0, notification);
  }

  public static boolean pref(Context context, String key, boolean defaut) {
    return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(key, defaut);
  }

  public static String pref(Context context, String key, String defaut) {
    return PreferenceManager.getDefaultSharedPreferences(context).getString(key, defaut);
  }

  public static int pref(Context context, String key, int defaut) {
    String as_string = pref(context, key, null);
    if(as_string == null) return defaut;
    return Integer.parseInt(as_string);
    //return PreferenceManager.getDefaultSharedPreferences(context).getInt(key, defaut);
  }

  public static long epoch(LocalDate date) {
    return date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
  }

  public static <K, V> Map<K, V> reduce_to(Map<K, V> map, int to_keep) {
    if(to_keep <= 0) return map;
    int to_remove = map.size() - to_keep;
    if(to_remove <= 0) return map;

    Iterator<Map.Entry<K,V>> itr = map.entrySet().iterator();
    for(int i = 0; i < to_remove; i++) {
      itr.next();
      itr.remove();
    }
    return map;
  }

  public static <V> List<V> reduce_to(List<V> list, int to_keep) {
    if(to_keep <= 0) return list;
    int to_remove = list.size() - to_keep;
    if(to_remove <= 0) return list;

    Iterator<V> itr = list.iterator();
    for(int i = 0; i < to_remove; i++) {
      itr.next();
      itr.remove();
    }
    return list;
  }

}
