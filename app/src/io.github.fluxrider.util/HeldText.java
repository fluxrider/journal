package io.github.fluxrider.util;

#include "way_too_many_imports" // see preprocessor.gradle

public class HeldText extends TextView {

  private Thread held = null;

  public HeldText(Context context, long period_start, long period_end, Runnable click) {
    super(context);

    setOnClickListener((View view) -> {
      click.run();
    });
    setLongClickable(true);
    setOnLongClickListener((View view) -> {
      held = new Thread(() -> {
        long period = period_start;
        while(held == Thread.currentThread()) {
          U.runOnUiThread(() -> {
            click.run();
          }); 
          SystemClock.sleep(period);
          // decay period towards min
          period = (long)(.9 * period + .1 * period_end);
        }
      });
      held.start();
      return true;
    });
    setOnTouchListener((View _v, MotionEvent e) -> {
      if(MotionEvent.ACTION_UP == e.getAction()) {
        held = null;
      }
      return false;
    });
  }

}
