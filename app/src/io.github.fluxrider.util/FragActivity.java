// The purpose of this class is to hide the mess of setting
// a placeholder and replacing it, when dealing with single
// fragment activities.

package io.github.fluxrider.util;

#include "way_too_many_imports" // see preprocessor.gradle

public abstract class FragActivity extends AppCompatActivity  {

  protected abstract Fragment newFragment();

  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setTheme(R.style.Theme_AppCompat_Light_DarkActionBar);

    FrameLayout layout = new FrameLayout(this);
    int frame = View.generateViewId();
    layout.setId(frame);
    layout.setLayoutParams(new ViewGroup.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.MATCH_PARENT
    ));
    setContentView(layout);

    getSupportFragmentManager()
      .beginTransaction()
      .replace(frame, newFragment())
      .commit();
  }

}
