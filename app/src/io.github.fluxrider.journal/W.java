package io.github.fluxrider.journal;

#include "way_too_many_imports" // see preprocessor.gradle

public class W {
  public LocalDate date;
  public double lb;

  public W() { }
  public W(LocalDate date, double lb) {
    this.date = date;
    this.lb = lb;
  }

  public int pack() {
    int year = date.getYear() - 2000; // 7bits [0-127] [2000-2127]
    int month = date.getMonthValue(); // 4 bits [1-12] 0,13,14,15 are unused
    int day = date.getDayOfMonth(); // 5 bits [1-31], 0 is unused
    int weight_lb_x10 = (int)(lb * 10); // 16 bits [0-10000], rest is unused, 2 bit wasted
    return year << 25 | month << 21 | day << 16 | weight_lb_x10;
  }

  public int unpack(byte [] buffer) { return unpack(buffer, 0); }
  public int unpack(byte [] buffer, int offset) {
    int date = ByteBuffer.wrap(buffer, offset, 2).getShort();
    int weight_lb_x10 = ByteBuffer.wrap(buffer, offset + 2, 2).getShort();
    int year = ((date >> 9) & 0xFF) + 2000;
    int month = (date >> 5) & 0xF;
    int day = date & 0b11111;
    this.date = LocalDate.of(year, month, day);
    this.lb = weight_lb_x10 / 10.0;
    return 4;
  }

  public String to_csv() {
    return String.format("%s,%03.1f", date.toString(), lb);
  }

  public String toString() {
    return String.format("%s: %3.1f lb %3.2f kg", date.toString(), lb, to_kg(lb));
  }

  public static double to_kg(double lb) {
    return 0.45359237 * lb;
  }

  public static List<W> from_file(Context context, boolean average) {
    List<W> out = new ArrayList<>();
    try {
      W w = new W();
      Map<LocalDate, Integer> count = new TreeMap<>();
      SortedMap<LocalDate, Double> map = new TreeMap<>();
      File file = new File(context.getFilesDir(), "weights.bin");
      if(file.exists()) {
        byte [] buffer = new byte[4];
        InputStream in = context.openFileInput("weights.bin");
        while(in.read(buffer) != -1) {
          w.unpack(buffer);
          // average dup dates
          if(average) {
            int n = 1;
            if(map.containsKey(w.date)) {
              n += count.get(w.date);
              w.lb = (w.lb + (map.get(w.date) * (n - 1))) / n;
            }
            map.put(w.date, w.lb);
            count.put(w.date, n);
          } else {
            out.add(w);
            w = new W();
          }
        }
        in.close();
      }
      if(average) {
        map.forEach((timestamp, w_lb) -> {
          out.add(new W(timestamp, w_lb));
        });
      }
      return out;
    } catch(IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void overwrite_file(Context context, List<W> data) {
    try {
      ByteBuffer buffer = ByteBuffer.allocate(4);
      OutputStream out = context.openFileOutput("weights.bin", Context.MODE_PRIVATE);
      for(W w : data) {
        buffer.putInt(0, w.pack());
        out.write(buffer.array());
      }
      out.close();
    } catch(IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static void append_to_file(Context context, W w) {
    try {
      ByteBuffer buffer = ByteBuffer.allocate(4);
      buffer.putInt(w.pack());
      OutputStream out = context.openFileOutput("weights.bin",
        Context.MODE_PRIVATE | Context.MODE_APPEND
      );
      out.write(buffer.array());
      out.close();
    } catch(IOException e) {
      throw new RuntimeException(e);
    }
  }

}
