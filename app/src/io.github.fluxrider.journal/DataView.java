package io.github.fluxrider.journal;

#include "way_too_many_imports" // see preprocessor.gradle
import androidx.recyclerview.widget.*;

public class DataView extends Activity {

  private List<W> data;
  private boolean dirty;

  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // data
    this.data = W.from_file(this, false);

    // empty case
    if(data.isEmpty()) {
      TextView text = new TextView(this);
      ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
        ViewGroup.LayoutParams.MATCH_PARENT,
        ViewGroup.LayoutParams.MATCH_PARENT
      );
      text.setGravity(Gravity.CENTER);
      text.setLayoutParams(params);
      text.setTextSize(18);
      text.setText("The file is empty.");
      setContentView(text);
      return;
    }

    // view
    RecyclerView recycler = new RecyclerView(this);
    recycler.setLayoutParams(new ViewGroup.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.MATCH_PARENT
    ));
    recycler.setLayoutManager(new LinearLayoutManager(this));
    recycler.setAdapter(new RecyclerView.Adapter<Holder>() {
      public int getItemCount() {
        return data.size();
      }

      public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LinearLayout layout = new LinearLayout(context);
        Holder holder = new Holder(layout);
        holder.context = context;
        LinearLayout.LayoutParams params;

        // date/weight text
        TextView text = holder.text = new TextView(context);
        params = new LinearLayout.LayoutParams(
          ViewGroup.LayoutParams.WRAP_CONTENT,
          ViewGroup.LayoutParams.MATCH_PARENT
        );
        params.gravity = Gravity.CENTER;
        params.weight = 1;
        text.setLayoutParams(params);
        text.setTextSize(18);
        text.setGravity(Gravity.CENTER);

        // delete button
        ImageButton button = holder.button = new ImageButton(context);
        params = new LinearLayout.LayoutParams(
          ViewGroup.LayoutParams.WRAP_CONTENT,
          ViewGroup.LayoutParams.MATCH_PARENT
        );
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.weight = 0;
        button.setLayoutParams(params);
        button.setImageResource(R.drawable.delete_outline);

        // layout
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.setLayoutParams(new ViewGroup.LayoutParams(
          ViewGroup.LayoutParams.MATCH_PARENT,
          ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        layout.addView(holder.text);
        layout.addView(holder.button);
        return holder;
      }

      public void onBindViewHolder(Holder holder, int position) {
        holder.text.setText(data.get(position).toString());
        holder.button.setOnClickListener((View v) -> {
          data.remove(holder.getAdapterPosition());
          notifyItemRemoved(holder.getAdapterPosition());
          dirty = true;
        });
      }
    });
    setContentView(recycler);
  }

  protected void onPause() {
    if(dirty) {
      W.overwrite_file(this, data);
      dirty = false;
    }
    super.onStop();
  }

  private static class Holder extends RecyclerView.ViewHolder {
    public TextView text;
    public ImageButton button;
    public Context context;
    public Holder(View view) {
      super(view);
    }
  }

}
