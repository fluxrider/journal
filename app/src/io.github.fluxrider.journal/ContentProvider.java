package io.github.fluxrider.journal;

#include "way_too_many_imports" // see preprocessor.gradle
import net.lingala.zip4j.*;

public class ContentProvider extends android.content.ContentProvider {

  public String getType(Uri uri) {
    return U.pref(getContext(), "zip", false)? "application/zip" : "text/csv";
  }

  private byte [] file_in_memory;

  // export to password protected zipped csv
  private void genFile() {
    boolean zip = U.pref(getContext(), "zip", false);
    String pass = U.pref(getContext(), "password", "");
    boolean use_password = pass.length() > 0;

    try {
      ByteArrayOutputStream bytes = new ByteArrayOutputStream();
      OutputStream out = bytes;

      if(zip) {
        ZipParameters params = new ZipParameters();
        if(use_password) {
          params.setEncryptFiles(true);
          params.setPassword(pass);
        }
        params.setFileNameInZip("weights.csv");
        out = new ZipOutputStream(out, params);
      }
      File file = new File(getContext().getFilesDir(), "weights.bin");
      if(file.exists()) {
        W w = new W();
        byte [] buffer = new byte[4];
        InputStream in = getContext().openFileInput("weights.bin");
        while(in.read(buffer) != -1) {
          w.unpack(buffer);
          out.write(String.format("%s\n", w.to_csv()).getBytes(StandardCharsets.UTF_8));
        }
        in.close();
      }
      out.close();
      file_in_memory = bytes.toByteArray();
    } catch(IOException e) {
      file_in_memory = null;
    }
  }

  // filename and size (but also gen file in memory, see comment below)
  public Cursor query(
    Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder
  ) {
    MatrixCursor cursor = new MatrixCursor(projection, 1);
    MatrixCursor.RowBuilder row = cursor.newRow();
    for(String column_name : projection) {
      Object value = null;
      switch(column_name) {
        case "_display_name":
          value = U.pref(getContext(), "zip", false)? "weights.zip" : "weights.csv";
          break;
        case "_size":
          // because I may zip, file must be generated now to know the final size
          genFile();
          if(file_in_memory != null) value = file_in_memory.length;
          break;
      }
      row.add(column_name, value);
    }

    return cursor;
  }

  // data
  public ParcelFileDescriptor openFile(Uri uri, String mode) throws FileNotFoundException {
    if(file_in_memory == null) genFile();
    if(file_in_memory == null)
      throw new FileNotFoundException("Could not create data for: " + uri.toString());
    try {
      // for reason I don't understand, the pipe must be created in openFile.
      // It cannot be created in query(). That's why I have a byte buffer.
      // Otherwise, I would have generated the file directly in the pipe.
      ParcelFileDescriptor [] pipe = ParcelFileDescriptor.createPipe();
      OutputStream out = new ParcelFileDescriptor.AutoCloseOutputStream(pipe[1]);
      out.write(file_in_memory);
      out.close();
      file_in_memory = null;
      return pipe[0];
    } catch(IOException e) {
      throw new RuntimeException(e);
    }
  }

  // Useless
  public boolean onCreate() {
    return true;
  }

  // This ain't a database
  public Uri insert(Uri uri, ContentValues values) {
    throw new UnsupportedOperationException("This ain't a database.");
  }

  public int delete(Uri uri, String selection, String[] selectionArgs) {
    throw new UnsupportedOperationException("This ain't a database.");
  }

  public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
    throw new UnsupportedOperationException("This ain't a database.");
  }

}
