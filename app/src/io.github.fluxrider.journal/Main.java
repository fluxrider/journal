package io.github.fluxrider.journal;

#include "way_too_many_imports" // see preprocessor.gradle
import android.app.AlertDialog;
import com.jjoe64.graphview.*;
import com.jjoe64.graphview.series.*;
import com.jjoe64.graphview.helper.*;

public class Main extends Activity {

  private SequencePicker date;
  private SequencePicker weight;
  private int graph_index;
  private LinearLayout layout;

  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    LinearLayout.LayoutParams params;

    // weight picker
    weight = new SequencePicker(this,
      getDefaultWeightForPicker(),
      (Object current, SequencePicker.ClickCallback onNeo) -> {
        EditText text = new EditText(this);
        text.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        new AlertDialog.Builder(this)
          .setTitle("Weight")
          .setMessage("Please enter your weight in pounds (lb).")
          .setView(text)
          .setPositiveButton(android.R.string.ok, (DialogInterface _d, int _b) -> {
            String user_input = text.getText().toString();
            int w_lb_x10 = (int)(Double.parseDouble(user_input) * 10);
            onNeo.update(w_lb_x10);
          })
          .show();
      },
      (Object current, boolean previous) -> {
        int w_lb_x10 = (Integer)current + (previous? -1 : 1);
        if(w_lb_x10 < 0) return current;
        if(w_lb_x10 > 10000) return current;
        return w_lb_x10;
      },
      (Object v) -> {
        double lb = (Integer)v / 10.0;
        return String.valueOf(String.format("%3.1f lb\n%3.2f kg", lb, W.to_kg(lb)));
      }
    );
    params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.WRAP_CONTENT
    );
    params.gravity = Gravity.CENTER_HORIZONTAL;
    params.weight = 1;
    weight.setLayoutParams(params);

    // date picker
    date = new SequencePicker(this,
      LocalDate.now(),
      (Object o, SequencePicker.ClickCallback onValue) -> {
        DatePickerDialog dialog = new DatePickerDialog(this);
        DatePicker picker = dialog.getDatePicker();
        picker.setMinDate(U.epoch(LocalDate.of(2000, 1, 1)));
        picker.setMaxDate(U.epoch(LocalDate.of(2127, 12, 31)));
        LocalDate t = (LocalDate)o;
        picker.updateDate(t.getYear(), t.getMonthValue() - 1, t.getDayOfMonth());
        dialog.setOnDateSetListener((DatePicker _v, int y, int m, int d) -> {
          onValue.update(LocalDate.of(y, m + 1, d));
        });
        dialog.show();
      },
      (Object o, boolean previous) -> {
        LocalDate t = (LocalDate)o;
        return t.plusDays(previous? -1 : 1);
      },
      (Object v) -> {
        return v.toString();
      }
    );
    params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.WRAP_CONTENT
    );
    params.weight = 1;
    params.gravity = Gravity.CENTER_HORIZONTAL;
    date.setLayoutParams(params);

    // graph stub (see refresh_graph)
    GraphView graph = new GraphView(this);
    params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      200
    );
    params.weight = 1;
    params.gravity = Gravity.CENTER_HORIZONTAL;
    graph.setLayoutParams(params);

    // save button
    ImageButton button = new ImageButton(this);
    params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.WRAP_CONTENT
    );
    params.gravity = Gravity.CENTER_HORIZONTAL;
    params.weight = 0;
    button.setLayoutParams(params);
    button.setImageResource(R.drawable.send);
    button.setOnClickListener((View v) -> {save(v);});

    // layout
    layout = new LinearLayout(this);
    layout.setOrientation(LinearLayout.VERTICAL);
    layout.setLayoutParams(new ViewGroup.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.MATCH_PARENT
    ));
    layout.addView(weight);
    layout.addView(date);
    layout.addView(graph);
    graph_index = layout.indexOfChild(graph);
    layout.addView(button);
    ScrollView scroll = new ScrollView(this);
    scroll.setFillViewport(true);
    scroll.setLayoutParams(new ViewGroup.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      ViewGroup.LayoutParams.MATCH_PARENT
    ));
    scroll.addView(layout);
    setContentView(scroll);
  }

  // com.jjoe64.graphview has very glitchy update. The workaround is to re-create the graph.
  private void refresh_graph() {
    GraphView graph = new GraphView(this);
    LineGraphSeries<DataPoint> series = new LineGraphSeries<>();
    U.reduce_to(W.from_file(this, true), U.pref(this, "to_show", 0)).forEach((w) -> {
      series.appendData(new DataPoint(U.epoch(w.date), w.lb), false, 10000, true); 
    });
    graph.addSeries(series);
    graph.getViewport().setScalable(true);
    graph.getViewport().setScrollable(true);
    graph.getGridLabelRenderer().setNumHorizontalLabels(4);
    graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
      public String formatLabel(double value, boolean isValueX) {
        if (isValueX) {
          LocalDateTime t = LocalDateTime.ofEpochSecond((long)(value/1000), 0, ZoneOffset.UTC);
          return String.format("%02d-%02d", t.getMonthValue(), t.getDayOfMonth());
        } else {
          return String.format("%3.1f", value);
        }
      }
    });
    graph.getGridLabelRenderer().setHumanRounding(false);
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
      ViewGroup.LayoutParams.MATCH_PARENT,
      200
    );
    params.weight = 1;
    params.gravity = Gravity.CENTER_HORIZONTAL;
    graph.setLayoutParams(params);

    layout.removeViewAt(graph_index);
    layout.addView(graph, graph_index);
  }

  protected void onResume () {
    super.onResume();
    refresh_graph();
  }

  public void save(View view) {
    // collect
    W w = new W((LocalDate)date.current_value, ((Integer)weight.current_value) / 10.0);

    // write
    W.append_to_file(this, w);
    U.toast(this, "Stored.");

    // update graph
    refresh_graph();
  }

  // Menu
  public boolean onCreateOptionsMenu(Menu menu) {
    // export button
    MenuItem export = menu.add("Export");
    export.setIcon(R.drawable.ic_menu_share_material);
    export.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    export.setOnMenuItemClickListener((MenuItem item) -> {
      boolean zip = U.pref(this, "zip", false);
      String pass = U.pref(this, "password", "");
      boolean use_password = pass.length() > 0;
      String email = U.pref(this, "email", "");

      // create intent to share (user chosen app will ask for stream)
      Intent intent = new Intent(Intent.ACTION_SEND);
      intent.setType(zip? "application/zip" : "text/csv");
      intent.putExtra(Intent.EXTRA_SUBJECT,
        "Weights " + LocalDate.now()
      );
      intent.putExtra(Intent.EXTRA_TEXT,
        "Please find attached your weights in a comma-separated values file" +
        (zip? (
        use_password?
        " (in a password protected zip)" :
        " (zipped)"
        ) : "") +
        ", which can be opened as a spreadsheet or plain text file.");
      if(email.length() > 0) intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
      intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
      // point to my content provider, which supplies the attachment stream
      intent.putExtra(Intent.EXTRA_STREAM,
        Uri.parse("content://io.github.fluxrider.journal/weights")
      );
      if(intent.resolveActivity(getPackageManager()) != null) {
        startActivity(Intent.createChooser(intent, "Export Weights"));
      }
      return true;
    });

    // edit preference
    MenuItem prefs = menu.add("Preference");
    prefs.setIcon(R.drawable.ic_settings_24dp);
    prefs.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    prefs.setOnMenuItemClickListener((MenuItem item) -> {
      startActivity(new Intent(this, Preference.class));
      return true;
    });

    // data view
    MenuItem data = menu.add("Data");
    data.setIcon(R.drawable.database_search);
    data.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
    data.setOnMenuItemClickListener((MenuItem item) -> {
      startActivity(new Intent(this, DataView.class));
      return true;
    });

    return true;
  }

  // Utils
  private int getDefaultWeightForPicker() {
    // parse latest entry of file, and use that weight as default.
    // I know latest doesn't mean with the latest timestamp, but whatever.
    // This is faster.
    File file = new File(getFilesDir(), "weights.bin");
    if(file.exists()) {
      long length = file.length();
      if(length > 0) {
        try {
          byte [] buffer = new byte[2];
          InputStream in = openFileInput("weights.bin");
          in.skip(length - 2);
          in.read(buffer);
          in.close();
          int last_weight_lb_x10 = ByteBuffer.wrap(buffer).getShort();
          return last_weight_lb_x10;
        } catch(Exception e) {
          U.toast(this, e.toString());
        }
      }
    }
    return 1800;
  }

}
