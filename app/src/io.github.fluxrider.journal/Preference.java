package io.github.fluxrider.journal;

#include "way_too_many_imports" // see preprocessor.gradle

public class Preference extends FragActivity  {

  protected Fragment newFragment() { return new Fragment(); }

  public static class Fragment extends PreferenceFrag {

    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
      Context context = getPreferenceManager().getContext();

      // export settings
      CheckBoxPreference zip = new CheckBoxPreference(context);
      zip.setKey("zip");
      zip.setTitle("Export as Zip");
      zip.setSummary("The exported csv will be zipped.");
      zip.setChecked(true);

      EditTextPreference password = new EditTextPreference(context);
      password.setKey("password");
      password.setTitle("Zip Password");
      password.setSummary("For password protected zip.");
      password.setOnBindEditTextListener((EditText e) -> {
        e.setInputType(
          InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD
        );
      });

      EditTextPreference email = new EditTextPreference(context);
      email.setKey("email");
      email.setTitle("Default Recipient");
      email.setOnBindEditTextListener((EditText e) -> {
        e.setInputType(
          InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        );
      });
      animate(email, "Default recipient email address when exporting data.");

      // graph settings
      EditTextPreference to_show = new EditTextPreference(context);
      to_show.setKey("to_show");
      to_show.setTitle("Number of samples");
      to_show.setOnBindEditTextListener((EditText e) -> {
        e.setInputType(InputType.TYPE_CLASS_NUMBER);
      });
      animate(to_show, "The number of point to plot on the graph.");
      to_show.setText("0");

      // categories
      PreferenceCategory export = new PreferenceCategory(context);
      export.setTitle("CSV Export");
      PreferenceCategory graph = new PreferenceCategory(context);
      graph.setTitle("Graph");

      // layout
      PreferenceScreen screen = getPreferenceManager().createPreferenceScreen(context);
      // warning: the android API requires categories to be added first,
      screen.addPreference(export);
      screen.addPreference(graph);
      // warning: and their children second.
      export.addPreference(zip);
      export.addPreference(password);
      export.addPreference(email);
      graph.addPreference(to_show);
      setPreferenceScreen(screen);
    }

  }

}
