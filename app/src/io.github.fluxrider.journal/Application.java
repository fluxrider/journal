package io.github.fluxrider.journal;

#include "way_too_many_imports" // see preprocessor.gradle

public class Application extends android.app.Application {

  public void onCreate() {
    super.onCreate();

    setTheme(android.R.style.Theme_Material_Light_DarkActionBar);

    Thread.setDefaultUncaughtExceptionHandler((Thread thread, Throwable t) -> {
      // print exception on stderr
      t.printStackTrace();

      // send as notification
      U.notification(this, "Crash Report", t);
    });
  }

}
